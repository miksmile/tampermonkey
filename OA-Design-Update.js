// ==UserScript==
// @name         OA Design Update
// @namespace    http://tampermonkey.net/
// @version      0.8
// @description  try to take over the world!
// @author       Maksym Mokrozub <maksym.mokrozub@oracle.com>
// @match        https://global-ebusiness.oraclecorp.com/OA_HTML/OA.jsp*
// @grant        none
// ==/UserScript==

(function() {
	'use strict';

	var style = document.createElement('style');
	style.textContent = 'td { border: none; } input { border-style: solid; border-width: 1px; }';
	document.body.appendChild(style);

})();
