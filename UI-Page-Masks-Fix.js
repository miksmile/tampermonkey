// ==UserScript==
// @name         UI Page Masks Fix
// @namespace    http://tampermonkey.net/
// @version      0.9
// @description  Fix tipsy popups.
// @author       Maksym Mokrozub <maksym.mokrozub@oracle.com>
// @match        https://ui61.maxymiser.com/CampaignBuilder/*
// @match        https://ui61us.maxymiser.com/CampaignBuilder/*
// @grant        none
// ==/UserScript==

var fixTips = function() {
	var selector = [
		'.mm-CampaignLocations .icon[data-ajax-url]',
		'.mm-CampaignTargeting .icon[data-ajax-url]',
		'.mm-ContentTargeting .icon[data-ajax-url]',
	].join(',');
	jQuery(selector).each(function() {
		try {
			jQuery(this).data().tipsy.options.ajax.type = 'GET';
		} catch (err) {}
	});
};

(function waiter() {
	if (typeof jQuery == 'function') {
		jQuery(fixTips);
	} else {
		setTimeout(waiter, 100);
	}
})();
