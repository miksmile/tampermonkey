// ==UserScript==
// @name         UI Chrome Login Fix
// @namespace    http://maxymiser.net/
// @version      1.0
// @description  try to take over the world!
// @author       Maksym Mokrozub
// @match        https://ui61us.maxymiser.com/Auth/Login*
// @match        https://ui61.maxymiser.com/Auth/Login*
// @grant        none
// ==/UserScript==
/* jshint -W097 */
'use strict';

(function waiter() {
	if (typeof jQuery == 'function' && jQuery('.text-center').length) {
		jQuery('form input[name*=Auto]').remove();
	} else {
		setTimeout(waiter, 100);
	}
})();

