// ==UserScript==
// @name         UI Site Setting Sort
// @namespace    maxymiser
// @version      1.1
// @description  sort labels in ui
// @author       miksmile
// @match        http*://*.maxymiser.com/*
// @grant        none
// ==/UserScript==

(function() {
	jQuery('.wrap.clearfix').each(function() {
		jQuery(this).append(jQuery(this)
			.children()
			.toArray()
			.sort(function(a, b) {
				var at = jQuery(a).text().trim();
				var bt = jQuery(b).text().trim();
				return at == bt ? 0 : at > bt ? 1 : -1;
			}));
	});
})();
